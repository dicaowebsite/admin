module.exports =
  {
    0: {
      header: true,
      title: 'dashboradmenus',
      hiddenOnCollapse: true
    },
    1: {
      href: '/dashboard',
      title: 'home',
      icon: 'icofont-home'
    },
    2: {
      title: 'Article',
      icon: 'icofont-document-folder',
      child: [
        {
          icon: 'icofont-ui-folder',
          href: '/dashboard/articlegroup',
          title: 'articlegroup'
        },
        {
          icon: 'icofont-listing-box',
          href: '/dashboard/articlelist',
          title: 'textlist'
        },
        {
          icon: 'icofont-label ',
          href: '/dashboard/hashtag',
          title: 'hashtag'
        }
      ]

    },
    3: {
      title: 'Setting',
      icon: 'icofont-ui-settings',
      child: [
        {
          icon: 'icofont-settings',
          href: '/dashboard/setting',
          title: 'Setting'
        },
       /* {
          icon: 'icofont-page',
          href: '/dashboard/firstpage',
          title: 'firstpage'
        },*/
       {
          icon: 'icofont-phone',
          href: '/dashboard/contactus',
          title: 'contactus'
        },
        /* {
          icon: 'icofont-info-circle',
          href: '/dashboard/about',
          title: 'aboutpage'
        }*/
      ]
    },
    4: {
      title: 'other',
      icon: 'icofont-star',
      child: [
        {
          icon: 'icofont-picture',
          href: '/dashboard/gallery',
          title: 'gallery'
        },
        {
          icon: 'icofont-info',
          href: '/dashboard/about',
          title: 'about'
        },
      ]
    },
   /* 5: {
      href: '',
      title: 'Banners',
      icon: 'icofont-ui-home',
      child: [
        {
          icon: 'icofont-ui-add ',
          href: '/dashboard/addbanner',
          title: 'addbanner'
        },
        {
          icon: 'icofont-list ',
          href: '/dashboard/listourbanners',
          title: 'listourbanners'
        },
        {
          icon: 'icofont-listine-dots ',
          href: '/dashboard/listallbanner',
          title: 'listallbanner'
        },
      ]
    },*/
   /* 6: {
      href: '',
      title: 'bannertools',
      icon: 'icofont-cubes',
      child: [
        {
          icon: 'icofont-location-pin ',
          href: '/dashboard/locations',
          title: 'locations'
        },
        {
          icon: 'icofont-ui-tag ',
          href: '/dashboard/typebanner',
          title: 'typebanner'
        },

      ]
    },*/
    7: {
      href: '',
      title: 'users',
      icon: 'icofont-users-alt-5',
      child: [
        {
          icon: 'icofont-listing-box ',
          href: '/dashboard/users',
          title: 'listusers'
        },
        {
          icon: 'icofont-ruler-pencil-alt-1 ',
          href: '/dashboard/post',
          title: 'post'
        },
      ]
    },
    8: {
      href: '',
      title: 'featureandAttributes',
      icon: 'icofont-toggle-on',
      child: [
        {
          icon: 'icofont-verification-check',
          href: '/dashboard/product/Attributes',
          title: 'Attributes'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/product/feature',
          title: 'feature'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/product/color',
          title: 'colormanager'
        },
      ],

    },
    9: {
      href: '',
      title: 'product',
      icon: 'icofont-box',
      child: [
        {
          icon: 'icofont-verification-check',
          href: '/dashboard/product/productgroup',
          title: 'productgroup'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/product',
          title: 'product'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/product/layoutproduct',
          title: 'layoutproduct'
        },

      ]
    },
   /* 10: {
      href: '',
      title: 'invoice',
      icon: 'icofont-list',
      child: [
        {
          icon: 'icofont-verification-check',
          href: '/dashboard/invoice',
          title: 'invoices'
        },

      ]
    },*/
    /*11: {
      href: '',
      title: 'payandinvoicesetting',
      icon: 'icofont-gear',
      child: [
        {
          icon: 'icofont-verification-check',
          href: '/dashboard/post/citypost',
          title: 'citysetting'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/post/postprice',
          title: 'postprice'
        },


      ]
    },*/
    12: {
      href: '',
      title: 'comment',
      icon: 'icofont-comment',
      child: [
        {
          icon: 'icofont-verification-check',
          href: '/dashboard/comment/commentsetting',
          title: 'commentsetting'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/comment/newcomment',
          title: 'newcomment'
        },
        {
          icon: 'icofont-verification-check ',
          href: '/dashboard/comment/archivecomment',
          title: 'archivecomment'
        }
      ]
    },
  }

    ;
